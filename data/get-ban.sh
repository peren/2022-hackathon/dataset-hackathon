#!/bin/sh

# Ensure data directory is created at root of this repository
cd "$(dirname "$0")"
cd ..
cd data

# Download BAN CSV file for Paris
wget -N https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/adresses-75.csv.gz
gunzip -k adresses-75.csv.gz
