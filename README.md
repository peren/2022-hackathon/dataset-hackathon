Dataset Hackathon
=================

Génération de données pour le hackathon


## Installation

```bash
python3 -m venv .venv
./.venv/bin/pip install -r requirements.txt
```
## Données synthetiques
Pour générer des donnés synthétiques, il faut télécharger la liste des adresses de paris en executant
```bash
chmod u+x data/get-ban.sh
data/get-ban.sh
```


## Utilisation

```bash
./.venv/bin/python3 __main__.py
```

## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte
de la licence se trouve dans le fichier LICENSE.md.
