import random
import uuid
from math import floor
from pathlib import Path
from typing import Union

import numpy as np
import pandas as pd


class AddressKeys:
    lat = "lat"
    lon = "lon"
    zip_code = "code_postal"


class SampledKeys:
    start_lat = "start_lat"
    start_lon = "start_lon"
    start_zip_code = f"start_{AddressKeys.zip_code}"
    end_lat = "end_lat"
    end_lon = "end_lon"
    end_zip_code = f"end_{AddressKeys.zip_code}"
    rain = "rain"
    heat = "heat"
    day_of_week = "day_of_week"
    month = "month"
    hour = "hour"
    minute = "minute"
    distance = "distance"
    demand = "demand"
    fee = "fee"


N_OBSERVATIONS, ADRESSES_BY_RESTAURANT = 4800, 8
RATIO_DISTANCE_FEE, RATIO_CONSTANT_FEE = 0.10, 0.05
MIN_DIST_RESTAURANT, MAX_DIST_RESTAURANT = 0.1, 5
RAIN_RANGE, RAIN_THRESHOLD = 10, 2
HEAT_RANGE, HEAT_THRESHOLD = 10, 1
ROUND_PRICES = True
MINIMUM_FEE, MAXIMUM_FEE, MAXIMUM_CONSTANT_FEE = 0.5, 5, 3
FIXED_SEED = 2

ADRESSES_COLUMNS = [AddressKeys.lat, AddressKeys.lon, AddressKeys.zip_code]

DAYS_OF_WEEK = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
]


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance in kilometers between two points
    on the earth (specified in decimal degrees)
    """
    lon1, lat1, lon2, lat2 = (
        np.radians(lon1),
        np.radians(lat1),
        np.radians(lon2),
        np.radians(lat2),
    )
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat / 2) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2) ** 2
    c = 2 * np.arcsin(np.sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles. Determines return value units.
    return c * r


def restaurant_seed(lat, lon):
    """Unique value by restaurant : the higher the more hype / expensive"""
    minimal_coeff = 0.25
    np.random.seed(seed=int((lat + lon) * 10000))
    return minimal_coeff + (1 - minimal_coeff) * np.random.rand()


def constant_fee_function(
    distance,
    start_attractivity,
    end_attractivity,
    demand,
    start_lat,
    start_lon,
    heat,
    rain,
    coeffs,
):
    """
    Generates constant (random) prices for a given location
    """
    return np.array(
        [
            MINIMUM_FEE
            + (MAXIMUM_CONSTANT_FEE - MINIMUM_FEE) * restaurant_seed(lat, lon)
            for lat, lon in zip(start_lat, start_lon)
        ]
    )


def distance_fee_function(
    distance,
    start_attractivity,
    end_attractivity,
    demand,
    start_lat,
    start_lon,
    heat,
    rain,
    coeffs,
):
    """
    Generates prices based only on distance
    """
    return 0.5 * np.sqrt(
        (
            np.array(
                [
                    10 * (restaurant_seed(lat, lon))
                    for lat, lon in zip(start_lat, start_lon)
                ]
            )
        )
        * distance
    )


def heat_influence(heat):
    heat_score = abs(heat - HEAT_RANGE / 2)
    if heat_score > HEAT_THRESHOLD:
        return (heat_score - HEAT_THRESHOLD) / (
            HEAT_RANGE / 2 - HEAT_THRESHOLD
        )  # ranges between 1 and 0
    return 0


def rain_influence(rain):
    if rain > RAIN_THRESHOLD:
        return (rain - RAIN_THRESHOLD) / (
            RAIN_RANGE - RAIN_THRESHOLD
        )  # ranges between 1 and 0
    return 0


def attractivity_influence(start_attractivity, end_attractivity):
    score = (
        start_attractivity / end_attractivity - 1.25
    )  # ratio ranges between 2 and 0.5
    if score > 0:
        return score / 0.75
    return 0


def custom_fee_function(
    distance,
    start_attractivity,
    end_attractivity,
    demand,
    start_lat,
    start_lon,
    heat,
    rain,
    coeffs,
):
    """
    Generates prices based on distance, with variable influence of weather (heat and rain),
    local attractivity and demand (influence of the hour of the day)
    """
    return (
        MINIMUM_FEE
        + coeffs[0] * np.array([heat_influence(heat_score) for heat_score in heat])
        + coeffs[1] * np.array([rain_influence(rain_score) for rain_score in rain])
        + coeffs[2]
        * np.array(
            [
                attractivity_influence(start_attr, end_attr)
                for start_attr, end_attr in zip(start_attractivity, end_attractivity)
            ]
        )
        + coeffs[3] * demand
    ) * distance_fee_function(
        distance,
        start_attractivity,
        end_attractivity,
        demand,
        start_lat,
        start_lon,
        heat,
        rain,
        coeffs,
    )


def get_synthetic_data(
    algorithm_label,
    destinations_addresses,
    restaurants_addresses,
    district_attractivity,
    target_function,
    coeffs=None,
):
    """
    Create synthetic data for a given fee function
    :param algorithm_label: label fot the batch
    :param sampled_addresses: (computed by get_synthetic_dataset)
    :param district_attractivity: (computed by get_synthetic_dataset)
    :param target_function: fee function used for the batch
    :return: a pandas dataframe with all columns
    """
    n_rows = len(destinations_addresses)
    start_sample = restaurants_addresses.rename(
        columns={key: f"start_{key}" for key in ADRESSES_COLUMNS}
    ).reset_index(drop=True)
    end_sample = destinations_addresses.rename(
        columns={key: f"end_{key}" for key in ADRESSES_COLUMNS}
    ).reset_index(drop=True)

    # All random features with fixed_seed
    np.random.seed(seed=FIXED_SEED)
    courses = pd.concat(
        [
            start_sample,
            end_sample,
            pd.DataFrame(
                {
                    SampledKeys.month: np.random.choice(range(1, 13), size=n_rows),
                    SampledKeys.rain: np.random.choice(
                        range(0, RAIN_RANGE), size=n_rows
                    ),
                    SampledKeys.heat: np.random.choice(
                        range(0, HEAT_RANGE), size=n_rows
                    ),
                    SampledKeys.day_of_week: np.random.choice(
                        DAYS_OF_WEEK, size=n_rows
                    ),
                    SampledKeys.hour: np.random.choice(range(12, 15), size=n_rows),
                    SampledKeys.minute: np.random.choice(range(0, 60), size=n_rows),
                }
            ),
        ],
        axis=1,
    )

    # Distance
    courses = courses.assign(
        **{
            SampledKeys.distance: haversine(
                courses["end_lon"],
                courses["end_lat"],
                courses["start_lon"],
                courses["start_lat"],
            )
        }
    )

    # Compute Demand (day / time dependent)
    courses = courses.assign(
        **{
            SampledKeys.demand: (
                np.sin(
                    (
                        (courses[SampledKeys.hour] + courses[SampledKeys.minute] / 60)
                        - 11
                    )
                    * np.pi
                    / 8
                )
            )
            * (0.8 if SampledKeys.day_of_week in ["Saturday", "Sunday"] else 1)
        }
    )

    courses = courses.assign(
        **{
            SampledKeys.fee: target_function(
                distance=courses[SampledKeys.distance],
                start_attractivity=courses[SampledKeys.start_zip_code].map(
                    district_attractivity
                ),
                end_attractivity=courses[SampledKeys.end_zip_code].map(
                    district_attractivity
                ),
                demand=courses[SampledKeys.demand],
                start_lat=courses[SampledKeys.start_lat],
                start_lon=courses[SampledKeys.start_lon],
                heat=courses[SampledKeys.heat],
                rain=courses[SampledKeys.rain],
                coeffs=coeffs,
            )
        }
    )

    courses["algorithm"] = np.zeros(n_rows, dtype=int) + algorithm_label
    return courses


def get_synthetic_dataset(
    addresses_path: Union[str, Path],
    n_rows=5000,
    addresses_by_restaurants=10,
    random_state=None,
    coeffs_for_custom_fee_functions=[(0, 0, 0, 1)],
):
    """
    Create synthetic dataset taken amongst random address in paris
    :param addresses_path: a path to a csv file with paris addresses
    :param n_rows: number of rows we want to obtain
    :param addresses_by_restaurants: number of destination adresses by restaurants
    :param random_state: a random seed that can be given
    :return: a pandas dataframe with all columns
    """

    # All adresses are computed at once. We want the district
    # attractivity to be the same for each fee function.
    all_addresses = pd.read_csv(addresses_path, delimiter=";")
    restaurants_addresses = all_addresses.sample(
        n=round(n_rows / addresses_by_restaurants),
        random_state=random_state,
    )[ADRESSES_COLUMNS].reset_index(drop=True)

    # Each restaurant is associated with <addresses_by_restaurants> destination addresses
    # These addresses are drawn uniformly from the addresses-75.csv and are filtered to remain within a n-kilometer
    # diameter disk around the restaurant
    destinations_addresses = pd.DataFrame()
    for _, restaurant in restaurants_addresses.iterrows():
        # Addresses are drawn from a frac of the dataset to preserve performance
        destinations = all_addresses.sample(frac=0.01, random_state=random_state)[
            ADRESSES_COLUMNS
        ]
        destinations = destinations.assign(
            **{
                SampledKeys.distance: haversine(
                    restaurant[AddressKeys.lon],
                    restaurant[AddressKeys.lat],
                    destinations[AddressKeys.lon],
                    destinations[AddressKeys.lat],
                )
            }
        )
        # "Hypeness" coefficient, specific to each restaurant
        restaurant_coeff = restaurant_seed(
            restaurant[AddressKeys.lat], restaurant[AddressKeys.lon]
        )
        distance_coeff = 0.5 + 0.5 * restaurant_coeff
        # Addresses within a disk depending of the hypeness of the restaurant are kept, the more hyper the bigger
        destinations = destinations[
            destinations[SampledKeys.distance] < distance_coeff * MAX_DIST_RESTAURANT
        ]
        destinations = destinations[
            destinations[SampledKeys.distance] > MIN_DIST_RESTAURANT
        ]
        destinations = destinations.sample(n=addresses_by_restaurants)
        destinations_addresses = pd.concat([destinations_addresses, destinations])

    restaurants_addresses = pd.concat(
        [restaurants_addresses] * addresses_by_restaurants
    ).sort_index()

    sampled_addresses = pd.concat([restaurants_addresses, destinations_addresses])
    districts = np.unique(sampled_addresses[AddressKeys.zip_code])

    np.random.seed(seed=FIXED_SEED)
    district_attractivity = {
        district: attractivity
        for district, attractivity in zip(
            districts, 0.5 * np.random.random(size=len(districts)) + 0.5
        )
    }

    # The number of points per custom fee functions is the same between custom functions
    n_custom_fee_functions = len(coeffs_for_custom_fee_functions)
    ratio_custom_fee = (
        1 - (RATIO_DISTANCE_FEE + RATIO_CONSTANT_FEE)
    ) / n_custom_fee_functions
    end_index_constant = round(RATIO_CONSTANT_FEE * n_rows)
    end_index_distance = end_index_constant + round(RATIO_DISTANCE_FEE * n_rows)

    ends_index_custom = [
        end_index_distance + round(n_rows * i * ratio_custom_fee)
        for i in range(n_custom_fee_functions + 1)
    ]

    custom_fee_synthetic_data = [
        get_synthetic_data(
            i + 3,
            destinations_addresses[ends_index_custom[i] : ends_index_custom[i + 1]],
            restaurants_addresses[ends_index_custom[i] : ends_index_custom[i + 1]],
            district_attractivity,
            custom_fee_function,
            coeffs=coeffs,
        )
        for i, coeffs in enumerate(coeffs_for_custom_fee_functions)
    ]

    courses = pd.concat(
        [
            get_synthetic_data(
                1,
                destinations_addresses[:end_index_constant],
                restaurants_addresses[:end_index_constant],
                district_attractivity,
                constant_fee_function,
            ),
            get_synthetic_data(
                2,
                destinations_addresses[end_index_constant:end_index_distance],
                restaurants_addresses[end_index_constant:end_index_distance],
                district_attractivity,
                distance_fee_function,
            ),
            *custom_fee_synthetic_data,
        ]
    )
    # fix seed for uuid
    rd = random.Random()
    rd.seed(0)
    courses["observation_uuid"] = [
        str(uuid.UUID(int=rd.getrandbits(128))) for _ in range(len(courses.index))
    ]

    return courses


def censure_dataset(dataset):
    """
    Censors the data to reproduce the difficulty of obtaining reliable data
    """

    # sample geo-location

    # (aroud 10 meters)
    localization_accuracy_meter = 10

    # calculation is done for the paris area
    round_for_lat = round(-np.log10(1 / 111.132 / 1000 * localization_accuracy_meter))
    round_for_lon = round(-np.log10(1 / 78.847 / 1000 * localization_accuracy_meter))

    dataset["start_lat"] = round(dataset["start_lat"], round_for_lat)
    dataset["start_lon"] = round(dataset["start_lon"], round_for_lon)

    dataset["end_lat"] = round(dataset["end_lat"], round_for_lat)
    dataset["end_lon"] = round(dataset["end_lon"], round_for_lon)

    dataset["fee"].where(dataset["fee"] <= MAXIMUM_FEE, MAXIMUM_FEE, inplace=True)
    dataset["fee"].where(dataset["fee"] >= MINIMUM_FEE, MINIMUM_FEE, inplace=True)

    def round_to_prices(x):
        return int(floor(x * 10) * 10 + 9) / 100

    if ROUND_PRICES:
        dataset["fee"] = dataset["fee"].apply(lambda x: round_to_prices(x))
    else:
        dataset["fee"] = dataset["fee"].apply(lambda x: int(x * 100) / 100)

    return dataset


if __name__ == "__main__":
    # data for control: all data
    df_for_control = get_synthetic_dataset(
        "./data/adresses-75.csv",
        n_rows=N_OBSERVATIONS,
        addresses_by_restaurants=ADRESSES_BY_RESTAURANT,
        random_state=FIXED_SEED,
        coeffs_for_custom_fee_functions=[
            # (heat, rain, attractivity, distance),
            (0, 0, 0, 0.6),
            (0, 0, 0.5, 0.5),
            (0.4, 0.4, 0.1, 0.3),
            (0.15, 0.15, 0.2, 0.5),
        ],
    )
    df_for_control = censure_dataset(df_for_control)
    df_for_control.to_csv("./data/for_control.csv", index=False)

    # for attendees
    df_for_candidate = df_for_control[
        [
            "observation_uuid",
            "start_lat",
            "start_lon",
            "start_code_postal",
            "end_lat",
            "end_lon",
            "end_code_postal",
            "rain",
            "heat",
            "day_of_week",
            "hour",
            "minute",
            "fee",
        ]
    ]
    # shuffle candidate dataset
    df_for_candidate = df_for_candidate.sample(frac=1, random_state=FIXED_SEED)
    df_for_candidate.to_csv("./data/for_candidate.csv", index=False)

    # data for leader board : just label an ID
    df_for_leader_board = df_for_control[["observation_uuid", "algorithm"]]
    df_for_leader_board.to_csv("./data/for_leader_board.csv", index=False)
